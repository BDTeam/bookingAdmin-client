import { NgModule, ModuleWithProviders } from '@angular/core';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {DatePickerComponent} from "./date-picker.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [
      CommonModule,
      NgbModule.forRoot(),
      FormsModule,
  ],
  declarations: [DatePickerComponent],
  exports: [
      DatePickerComponent
  ]
})
export class DatePickerModule {
}
