import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  imports: [
    CommonModule,
      NgbModule.forRoot(),
  ],
  declarations: [PaginationComponent],
  exports: [
    PaginationComponent
  ]
})
export class PaginationModule {
}
