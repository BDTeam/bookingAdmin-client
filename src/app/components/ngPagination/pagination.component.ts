import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
    defaultPagination: number;
    advancedPagination: number;
    paginationSize: number;
    disabledPagination: number;
    isDisabled: boolean;

    constructor() {
        this.defaultPagination = 0;
        this.advancedPagination = 1;
        this.paginationSize = 5;
        this.disabledPagination = 1;
        this.isDisabled = true;
    }

    toggleDisabled() {
        this.isDisabled = !this.isDisabled;
    }

}
