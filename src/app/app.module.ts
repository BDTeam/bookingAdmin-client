import {Injectable, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {BrowserXhr, Http, HttpModule} from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { HttpService } from '../providers/http-service';
import {StorageService} from "../providers/storage-service";
import {OrderService} from "../services/OrderService";
import {DatePipe} from "@angular/common";
import {PreOrderService} from "../services/PreOrderService";
import {MembersService} from "../services/MembersService";
import {UserService} from "../services/UserService";
import {IncomeService} from "../services/IncomeService";
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-4/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}
// @Injectable()
// export class CorsBrowserXhr extends BrowserXhr {
//     constructor() {
//         super();
//     }
//
//     build(): any {
//         let xhr:XMLHttpRequest = super.build();
//         xhr.withCredentials = true;
//         return <any>(xhr);
//     }
// }

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        })
    ],
    providers: [
        // { provide: BrowserXhr, useClass:CorsBrowserXhr },
        AuthGuard,
        HttpService,
        MembersService,
        StorageService,
        OrderService,
        PreOrderService,
        DatePipe,
        UserService,
        IncomeService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
