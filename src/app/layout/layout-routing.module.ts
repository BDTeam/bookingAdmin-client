import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'members', loadChildren: './members/members.module#MembersModule' },
            { path: 'wxmembers', loadChildren: './wxmembers/wxmembers.module#WxMembersModule' },
            { path: 'pre-order', loadChildren: './pre-order/pre-order.module#PreOrderModule' },
            { path: 'order', loadChildren: './order/order.module#OrderModule' },
            { path: 'wpservice', loadChildren: './wpservice/wpService.module#WpserviceModule' },
            { path: 'user', loadChildren: './user/user.module#UserModule' },
            { path: 'income-detail', loadChildren: './income-detail/income-detail.module#IncomeDetailModule' },
            { path: 'income-statistics', loadChildren: './income-statistics/income-statistics.module#IncomeStatisticsModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
