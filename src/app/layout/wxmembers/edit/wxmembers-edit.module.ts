import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WxMembersEditRoutingModule } from './wxmembers-edit-routing.module';
import { WxMembersEditComponent } from './wxmembers-edit.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        WxMembersEditRoutingModule,
        PageHeaderModule,
    ],
    declarations: [WxMembersEditComponent]
})
export class WxMembersEditModule { }
