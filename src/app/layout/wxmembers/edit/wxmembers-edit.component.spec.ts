import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {WxMembersEditComponent} from "./wxmembers-edit.component";

describe('WxWxMembersEditComponent', () => {
  let component: WxMembersEditComponent;
  let fixture: ComponentFixture<WxMembersEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      PageHeaderModule,
    ],
      declarations: [ WxMembersEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WxMembersEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
