import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MembersService} from "../../../../services/MembersService";
import {WechatUsers} from "../../../../domain/WechatUsers";

@Component({
    selector: 'app-wxmembers-edit',
    templateUrl: './wxmembers-edit.component.html',
    styleUrls: ['./wxmembers-edit.component.scss'],
    animations: [routerTransition()]
})
export class WxMembersEditComponent implements OnInit {
    myForm: FormGroup;
    id:number;
    wxuserName:string="";
    remark:string="";
    wechatUsers:WechatUsers={};
    constructor( fb: FormBuilder,private activeRoute: ActivatedRoute,private membersService: MembersService,public router: Router) {
        this.myForm = fb.group({
            'id': [''],
            'remark': ['', Validators.required],
        });
    }
    //存储错误信息
    formErrors = {
        'remark': '',
    };
    //错误对应的提示
    validationMessages = {
        'remark': {
            'required': '必填.',
        },
    };
    /**
     * 表单值改变时，重新校验
     * @param data
     */
    onValueChanged(data) {
        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            //取到表单字段
            const control = this.myForm.get(field);
            //表单字段已修改或无效
            if (control && control.dirty && !control.valid) {
                //取出对应字段可能的错误信息
                const messages = this.validationMessages[field];
                //从errors里取出错误类型，再拼上该错误对应的信息
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + '';
                }
            }

        }

    }

    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => { this.id = params["userId"]}
        );
        this.getWxUserInfo();
    }
    getWxUserInfo(){
        this.membersService.selectWxMembersById(this.id).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.wechatUsers = restEntity.object;
                this.id =this.wechatUsers.id;
                this.wxuserName = this.wechatUsers.nickName;
                this.remark = this.wechatUsers.remark;
            }
        }).catch(
            error => console.log(error)
        )
    }
    setRemark(value:string){
        console.log(value);
        this.membersService.setRemark(value).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.router.navigate(['/wxmembers']);
            }
        }).catch(
            error => console.log(error)
        )
    }

}
