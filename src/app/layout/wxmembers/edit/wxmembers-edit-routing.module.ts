import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WxMembersEditComponent} from "./wxmembers-edit.component";

const routes: Routes = [
    { path: 'wxmembers-edit/:userId', component: WxMembersEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WxMembersEditRoutingModule { }
