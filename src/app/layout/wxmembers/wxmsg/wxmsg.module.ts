import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WxMsgRoutingModule } from './wxmsg-routing.module';
import { WxMsgComponent } from './wxmsg.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DatePickerModule} from "../../../components/date/date-picker.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        WxMsgRoutingModule,
        PageHeaderModule,
    ],
    declarations: [WxMsgComponent]
})
export class WxMsgModule { }
