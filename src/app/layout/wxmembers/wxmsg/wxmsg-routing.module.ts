import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WxMsgComponent} from "./wxmsg.component";

const routes: Routes = [
    { path: 'wxmsg/:userId', component: WxMsgComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WxMsgRoutingModule { }
