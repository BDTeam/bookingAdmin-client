import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WxMembersComponent } from './wxmembers.component';
import { WxMembersRoutingModule } from './wxmembers-routing.module';
import { PageHeaderModule } from './../../shared';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
import {WxMembersEditModule} from "./edit/wxmembers-edit.module";
import {WxMsgModule} from "./wxmsg/wxmsg.module";
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule ,
        WxMembersRoutingModule,
        PageHeaderModule,
        // ngPaginationModule,
        PaginationModule,
        WxMembersEditModule,
        WxMsgModule,
    ],
    declarations: [WxMembersComponent]
})
export class WxMembersModule { }
