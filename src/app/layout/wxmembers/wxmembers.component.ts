import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {MembersService} from "../../../services/MembersService";
import {WechatUsers} from "../../../domain/WechatUsers";

@Component({
    selector: 'app-wxmembers',
    templateUrl: './wxmembers.component.html',
    styleUrls: ['./wxmembers.component.scss'],
    animations: [routerTransition()]
})
export class WxMembersComponent implements OnInit {
    data: WechatUsers[];
    params: Array<{ key: string, value: any }>;//查询条件
    nickname?: string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    constructor(private membersService: MembersService) {
    }
    ngOnInit() {
        this.searchList();
    }
    searchList() {
        if(this.nickname!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "nickname", value: this.nickname},{key: "page", value: pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.membersService.selectWxMembersByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.data = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }

    selectPage() {
        // console.log(this.paginationComponent.defaultPagination)
        this.searchList();
    }

    /***
     * 同步微信数据
     */
    weChat_sync(){
        this.membersService.weChatSync(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                console.log(restEntity.msg);
            }
        }).catch(
            error => console.log(error)
        )
    }
}
