import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WxMembersComponent } from './wxmembers.component';

const routes: Routes = [
    { path: '', component: WxMembersComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WxMembersRoutingModule { }
