import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {HttpService} from "../../../../providers/http-service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {PreOrder} from "../../../../domain/PreOrder";
import {DatePickerComponent} from "../../../components/date/date-picker.component";
import {WPService} from "../../../../domain/WPService";
import {Order} from "../../../../domain/Order";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OrderService} from "../../../../services/OrderService";
import {EnumObj} from "../../../../domain/EnumObj";

@Component({
    selector: 'app-order-edit',
    templateUrl: './order-edit.component.html',
    styleUrls: ['./order-edit.component.scss'],
    animations: [routerTransition()]

})
export class OrderEditComponent implements OnInit {
    myForm: FormGroup;
    orderId: string;
    dinnerService: WPService = {};
    additionalService: WPService = {};
    order?: Order = {};
    dinner: string = "";
    additionals: any[] = [];
    services: WPService[] = [];
    paymentCodes:EnumObj[] =[];
    orderChannels:EnumObj[] =[];
    @ViewChild(DatePickerComponent) datePickerComponent: DatePickerComponent;

    constructor(private httpService: HttpService, private activeRoute: ActivatedRoute, private router: Router, fb: FormBuilder, private orderService: OrderService,) {
        this.myForm = fb.group({
            'userName': ['', Validators.required],
            'phone': ['', Validators.required],
            'orderTime': ['', Validators.required],
            'startTime': ['', Validators.required],
            'endTime': ['', Validators.required],
            'diningNum': ['', Validators.required],
            'dinners': ['', Validators.required],
            'additionals': [''],
            'orderRemark': [''],
            'orderAmount': [''],
            'earnestAmount': [''],
            'retainageAmount': [''],
            'orderChannel': [''],
            'earnestPayMethod': [''],
            'retainagePayMethod': [''],
            'id': [],
        });

    }

    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => {
                this.orderId = params["orderId"]
            }
        );
        this.queryPreOrder();
    }

    queryPreOrder() {
        this.httpService.httpGetWithAuth("/order/getInfo?orderId=" + this.orderId).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.order = restEntity.object;
                this.datePickerComponent.model = this.order.orderTime;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/wPService/serviceTypeList?serviceType=2").then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.dinnerService = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/wPService/serviceTypeList?serviceType=0").then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.additionalService = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/order/orderDetailList?orderId=" + this.orderId).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.services = restEntity.object;
                console.log(this.services)
                for (let service of this.services) {
                    if (service.service_type == "2") {
                        this.dinner = service.id + "";
                    } else {
                        this.additionals.push(service.id + "");
                    }
                }
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/order/getOrderPayMethodList").then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.paymentCodes = restEntity.object;
                console.log(restEntity);
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/order/getOrderChannelList").then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.orderChannels = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
    }

    editOrder(value: string) {
        let dateObj = eval(this.datePickerComponent.model);
        console.log(dateObj);
        if (dateObj != null && dateObj.year) {
            this.datePickerComponent.model = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
            let orderObj = eval(value);
            orderObj.orderTime = this.datePickerComponent.model;
            orderObj.additionals = this.additionals;
        } else {
            let orderObj = eval(value);
            orderObj.orderTime = this.datePickerComponent.model;
            orderObj.additionals = this.additionals;
        }
        this.orderService.editOrder(value).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "修改失败");
                return;
            } else {
                this.router.navigate(['/order']);
            }
        }).catch(
            error => {
                console.log("修改失败")
            }
        );
    }

    onChange(event) {
        var additionalIsChecked = event.currentTarget.checked;
        var additionalValue = event.currentTarget.value.trim() + "";
        if (additionalIsChecked) {
            if (this.additionals.indexOf(additionalValue + "") < 0) {
                this.additionals.push(additionalValue + "")
            }
        } else if (!additionalIsChecked) {
            let i = this.additionals.indexOf(additionalValue + "");
            this.additionals.splice(i, 1);
        }
    }

    isSelected(id: any, type: any) {
        let res: Boolean = false;
        if (type == "2") {
            if (this.dinner == id) {
                res = true;
                return res;
            }
        } else if (type == "0") {
            if (this.additionals.indexOf(id + "") >= 0) {
                res = true;
                return res;
            }
        }
        return res;
    }


}
