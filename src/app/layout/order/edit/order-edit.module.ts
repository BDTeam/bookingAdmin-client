import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderEditRoutingModule } from './order-edit-routing.module';
import { OrderEditComponent } from './order-edit.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DatePickerModule} from "../../../components/date/date-picker.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        OrderEditRoutingModule,
        PageHeaderModule,
        DatePickerModule,
    ],
    declarations: [OrderEditComponent,]
})
export class OrderEditModule { }
