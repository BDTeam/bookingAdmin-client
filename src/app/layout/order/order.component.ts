import {Component, OnInit, ViewChild} from '@angular/core';
import {Order} from "../../../domain/Order";
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {OrderService} from "../../../services/OrderService";
import {DatePipe} from "@angular/common";
import {routerTransition} from "../../router.animations";

@Component({
    selector: 'app-order-page',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
    animations: [routerTransition()]
})
export class OrderComponent implements OnInit {
    data: Order[];
    params: Array<{ key: string, value: any }>;//查询条件
    phone?: string="";
    userName?: string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    constructor(private orderService:OrderService,private datePipe :DatePipe,) {

    }

    ngOnInit() {
        this.searchList();
    }
    searchList() {
        if(this.phone!=''||this.userName!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "phone", value: this.phone},{key: "userName", value: this.userName},{key: "page", value: pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.orderService.selectByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.data = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }
    selectPage() {
        // console.log(this.paginationComponent.defaultPagination)
        this.searchList();
    }

    delOrder(value:any){
        console.log(value);
        if(value){
            this.params = [{key: "orderId", value: value}];
            this.orderService.delOrder(this.params).then(restEntity => {
                if (restEntity.status ==-1) {
                    console.log(restEntity.msg, "删除失败");
                    return;
                }else{
                    this.searchList();
                }
            }).catch(
                error => {
                    console.log("删除失败")
                }
            );
        }
    }
}
