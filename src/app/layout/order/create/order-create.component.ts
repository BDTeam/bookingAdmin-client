import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {HttpService} from "../../../../providers/http-service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {PreOrder} from "../../../../domain/PreOrder";
import {DatePickerComponent} from "../../../components/date/date-picker.component";
import {WPService} from "../../../../domain/WPService";
import {Order} from "../../../../domain/Order";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OrderService} from "../../../../services/OrderService";
import {PreOrderService} from "../../../../services/PreOrderService";

@Component({
    selector: 'app-order-create',
    templateUrl: './order-create.component.html',
    styleUrls: ['./order-create.component.scss'],
    animations: [routerTransition()]

})
export class OrderCreateComponent implements OnInit {
    myForm: FormGroup;
    preOrderId:string;
    preOrder:PreOrder={};
    dinnerService:WPService={};
    additionalService:WPService={};
    order?:Order={};
    dinners:any[]=[];
    additionals:any[]=[];
    @ViewChild(DatePickerComponent) datePickerComponent: DatePickerComponent;
    constructor(private httpService: HttpService,private activeRoute: ActivatedRoute,private router: Router, fb: FormBuilder,private orderService:OrderService,private preOrderService:PreOrderService) {
        this.myForm = fb.group({
            'userName': ['', Validators.required],
            'phone': ['', Validators.required],
            'orderTime': ['', Validators.required],
            'startTime': ['', Validators.required],
            'endTime': ['', Validators.required],
            'diningNum': ['', Validators.required],
            'dinners': ['', Validators.required],
            'additionals': ['', Validators.required],
            'userId': [''],
        });
    }
    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => { this.preOrderId = params["preOrderId"]}
        );
        this.queryPreOrder();
    }
    queryPreOrder(){
        this.httpService.httpGetWithAuth("/preOrder/getInfo?orderId="+this.preOrderId).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.preOrder = restEntity.object;
                this.order.userName=this.preOrder.contactsName;
                this.order.phone = this.preOrder.contactsTel;
                this.order.startTime = this.preOrder.startTime;
                this.order.endTime = this.preOrder.endTime;
                this.order.diningNum = this.preOrder.nums;
                this.datePickerComponent.model=this.preOrder.startDate;
                this.order.userId = this.preOrder.createId;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/wPService/serviceTypeList?serviceType=2").then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.dinnerService = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/wPService/serviceTypeList?serviceType=0").then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.additionalService = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
    }
    onChange(event) {
        var additionalIsChecked = event.currentTarget.checked;
        var additionalValue = event.currentTarget.value.trim();
        if( additionalIsChecked ) {
            this.additionals.push(additionalValue)
        }else if( !additionalIsChecked ) {
            let i = this.additionals.indexOf(additionalValue);
            this.additionals.splice(i, 1);
        }
        console.log(this.additionals);
    }
    addOrder(value: string){

        let dateObj = eval(this.datePickerComponent.model);
        console.log(dateObj);
        if(dateObj!=null&&dateObj.year){
            this.datePickerComponent.model = dateObj.year+'-'+dateObj.month+'-'+dateObj.day;
            let orderObj = eval(value);
            orderObj.orderTime = this.datePickerComponent.model;
            orderObj.additionals = this.additionals;
        }else{
            let orderObj = eval(value);
            orderObj.orderTime = this.datePickerComponent.model;
            orderObj.additionals = this.additionals;
        }
        console.log(value);
        console.log(this.order);
        console.log(this.dinners);
        console.log(this.myForm.get('additionals'))
        this.orderService.addOrder(value).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "新增失败");
                return;
            }else{
                console.log("更新成功")
                this.preOrder.orderState ="04";
                this.preOrderService.editPreOrder(this.preOrder).then(restEntity => {
                    if (restEntity.status ==1) {
                        this.router.navigate(['/order']);
                    }
                }).catch(
                    error => {
                        console.log("更新失败")
                    }
                );
            }
        }).catch(
            error => {
                console.log("新增失败")
            }
        );

    }
}
