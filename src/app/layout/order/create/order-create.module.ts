import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderCreateRoutingModule } from './order-create-routing.module';
import { OrderCreateComponent } from './order-create.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DatePickerModule} from "../../../components/date/date-picker.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        OrderCreateRoutingModule,
        PageHeaderModule,
        DatePickerModule,
    ],
    declarations: [OrderCreateComponent,]
})
export class OrderCreateModule { }
