import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrderRoutingModule} from './order-routing.module';
import {OrderComponent} from './order.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
import {OrderCreateModule} from "./create/order-create.module";
import {OrderEditModule} from "./edit/order-edit.module";
import {OrderViewModule} from "./view/order-view.module";

@NgModule({
    imports: [
        CommonModule,
        OrderRoutingModule,
        FormsModule,
        PageHeaderModule,
        ReactiveFormsModule,
        OrderCreateModule,
        OrderEditModule,
        OrderViewModule,
        PaginationModule,
    ],
    declarations: [OrderComponent]
})
export class OrderModule {
}
