import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {HttpService} from "../../../../providers/http-service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {PreOrder} from "../../../../domain/PreOrder";
import {DatePickerComponent} from "../../../components/date/date-picker.component";
import {WPService} from "../../../../domain/WPService";
import {Order} from "../../../../domain/Order";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OrderService} from "../../../../services/OrderService";
import {EnumObj} from "../../../../domain/EnumObj";

@Component({
    selector: 'app-order-view',
    templateUrl: './order-view.component.html',
    styleUrls: ['./order-view.component.scss'],
    animations: [routerTransition()]

})
export class OrderViewComponent implements OnInit {
    myForm: FormGroup;
    orderId:string;
    dinnerService:WPService={};
    additionalService:WPService={};
    order?:Order={};
    dinners:any[]=[];
    additionals:any[]=[];
    paymentCodes:EnumObj[] =[];
    orderChannels:EnumObj[] =[];
    @ViewChild(DatePickerComponent) datePickerComponent: DatePickerComponent;
    constructor(private httpService: HttpService,private activeRoute: ActivatedRoute) {

    }
    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => { this.orderId = params["orderId"]}
        );
        this.queryPreOrder();
    }
    queryPreOrder(){
        this.httpService.httpGetWithAuth("/order/getInfo?orderId="+this.orderId).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.order = restEntity.object;
                this.datePickerComponent.model=this.order.orderTime;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/order/orderDetailList?serviceType=2&orderId="+this.orderId).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.dinnerService = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/order/orderDetailList?serviceType=0&orderId="+this.orderId).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.additionalService = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/order/getOrderPayMethodList").then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.paymentCodes = restEntity.object;
                console.log(restEntity);
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
        this.httpService.httpGetWithAuth("/order/getOrderChannelList").then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg, "获取失败");
                return;
            } else {
                this.orderChannels = restEntity.object;
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
    }
}
