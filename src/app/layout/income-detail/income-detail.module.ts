import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IncomeDetailRoutingModule} from './income-detail-routing.module';
import {IncomeDetailComponent} from './income-detail.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
import {DatePickerModule} from "../../components/date/date-picker.module";

@NgModule({
    imports: [
        CommonModule,
        IncomeDetailRoutingModule,
        FormsModule,
        PageHeaderModule,
        ReactiveFormsModule,
        PaginationModule,
        DatePickerModule,
    ],
    declarations: [IncomeDetailComponent]
})
export class IncomeDetailModule {
}
