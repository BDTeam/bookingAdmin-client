import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {IncomeDetailViewComponent} from "./income-detail-view.component";

const routes: Routes = [
    { path: 'income-detail-view/:orderId', component: IncomeDetailViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncomeDetailViewRoutingModule { }
