import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {IncomeDetailViewRoutingModule} from './income-detail-view-routing.module';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DatePickerModule} from "../../../components/date/date-picker.module";
import {IncomeDetailViewComponent} from "./income-detail-view.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IncomeDetailViewRoutingModule,
        PageHeaderModule,
        DatePickerModule,
    ],
    declarations: [IncomeDetailViewComponent]
})
export class IncomeDetailViewModule { }
