import {Component, OnInit, ViewChild} from '@angular/core';
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {IncomeService} from "../../../services/IncomeService";
import {routerTransition} from "../../router.animations";
import {IncomeDetailVO} from "../../../domain/IncomeDetailVO";
import {DatePickerComponent} from "../../components/date/date-picker.component";

@Component({
    selector: 'app-incomeDetail-page',
    templateUrl: './income-detail.component.html',
    styleUrls: ['./income-detail.component.scss'],
    animations: [routerTransition()]
})
export class IncomeDetailComponent implements OnInit {
    data: IncomeDetailVO[];
    params: Array<{ key: string, value: any }>;//查询条件
    startDate?: string="";
    endDate?: string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    @ViewChild(DatePickerComponent) datePickerComponent: DatePickerComponent;
    constructor(private incomeDetailService:IncomeService) {

    }

    ngOnInit() {
        this.searchList();
    }
    searchList() {
        if(this.startDate!=''||this.endDate!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "startDate", value: this.startDate},{key: "endDate", value: this.endDate},{key: "page", value: pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.incomeDetailService.selectIncomeDetailByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.data = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }
    selectPage() {
        // console.log(this.paginationComponent.defaultPagination)
        this.searchList();
    }
}
