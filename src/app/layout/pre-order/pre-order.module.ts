import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PreOrderRoutingModule} from './pre-order-routing.module';
import {PreOrderComponent} from './pre-order.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
import {PreOrderEditModule} from "./edit/pre-order-edit.module";
import {PreOrderViewModule} from "./view/pre-order-view.module";

@NgModule({
    imports: [
        CommonModule,
        PreOrderRoutingModule,
        FormsModule,
        PageHeaderModule,
        ReactiveFormsModule,
        PreOrderEditModule,
        PaginationModule,
        PreOrderViewModule,
    ],
    declarations: [PreOrderComponent]
})
export class PreOrderModule {
}
