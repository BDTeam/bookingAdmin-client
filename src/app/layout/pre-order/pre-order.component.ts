import {Component, OnInit, ViewChild} from '@angular/core';
import {PreOrder} from "../../../domain/PreOrder";
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {PreOrderService} from "../../../services/PreOrderService";
import {routerTransition} from "../../router.animations";

@Component({
    selector: 'app-pre-order-page',
    templateUrl: './pre-order.component.html',
    styleUrls: ['./pre-order.component.scss'],
    animations: [routerTransition()]
})
export class PreOrderComponent implements OnInit {
    data: PreOrder[];
    params: Array<{ key: string, value: any }>;//查询条件
    contactsTel?: string="";
    contactsName?: string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    constructor(private preOrderService:PreOrderService) {
    }
    ngOnInit() {
        this.searchList();
    }
    searchList() {
        if(this.contactsTel!=''||this.contactsName!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "contactsTel", value: this.contactsTel},{key: "contactsName", value: this.contactsName},{key: "page", value: pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.preOrderService.selectByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.data = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }
    selectPage() {
        // console.log(this.paginationComponent.defaultPagination)
        this.searchList();
    }

    delPreOrder(value:any){
        console.log(value);
        if(value){
            this.params = [{key: "id", value: value}];
            this.preOrderService.delPreOrder(this.params).then(restEntity => {
                if (restEntity.status ==-1) {
                    console.log(restEntity.msg, "删除失败");
                    return;
                }else{
                    this.searchList();
                }
            }).catch(
                error => {
                    console.log("删除失败")
                }
            );
        }
    }
}
