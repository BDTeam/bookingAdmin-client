import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreOrderViewRoutingModule } from './pre-order-view-routing.module';
import { PreOrderViewComponent } from './pre-order-view.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DatePickerModule} from "../../../components/date/date-picker.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PreOrderViewRoutingModule,
        PageHeaderModule,
        DatePickerModule,
    ],
    declarations: [PreOrderViewComponent]
})
export class PreOrderViewModule { }
