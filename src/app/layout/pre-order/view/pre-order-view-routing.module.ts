import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PreOrderViewComponent} from "./pre-order-view.component";

const routes: Routes = [
    { path: 'pre-order-view/:orderId', component: PreOrderViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreOrderViewRoutingModule { }
