import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {PreOrderViewComponent} from "./pre-order-view.component";

describe('PreOrderViewComponent', () => {
  let component: PreOrderViewComponent;
  let fixture: ComponentFixture<PreOrderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      PageHeaderModule,
    ],
      declarations: [ PreOrderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreOrderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
