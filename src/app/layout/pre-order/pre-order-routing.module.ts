import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreOrderComponent } from './pre-order.component';
import {PreOrderEditComponent} from "./edit/pre-order-edit.component";

const routes: Routes = [
    { path: '', component: PreOrderComponent,
        // children: [
        //     { path: 'pre-order-edit/:orderId',component:PreOrderEditComponent },
        // ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PreOrderRoutingModule { }
