import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreOrderEditRoutingModule } from './pre-order-edit-routing.module';
import { PreOrderEditComponent } from './pre-order-edit.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DatePickerModule} from "../../../components/date/date-picker.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PreOrderEditRoutingModule,
        PageHeaderModule,
        DatePickerModule,
    ],
    declarations: [PreOrderEditComponent]
})
export class PreOrderEditModule { }
