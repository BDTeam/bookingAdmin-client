import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {HttpService} from "../../../../providers/http-service";
import {PreOrder} from "../../../../domain/PreOrder";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PreOrderService} from "../../../../services/PreOrderService";
import {DatePickerComponent} from "../../../components/date/date-picker.component";
import {PreOrderTrack} from "../../../../domain/PreOrderTrack";

@Component({
    selector: 'app-pre-order-edit',
    templateUrl: './pre-order-edit.component.html',
    styleUrls: ['./pre-order-edit.component.scss'],
    animations: [routerTransition()]
})
export class PreOrderEditComponent implements OnInit {
    myForm: FormGroup;
    orderId:string;
    preOrder?:PreOrder;
    orderStates=[];
    orderState:string="00";
    preOrderTracks:PreOrderTrack[];
    @ViewChild(DatePickerComponent) datePickerComponent: DatePickerComponent;
    constructor(private httpService: HttpService,private activeRoute: ActivatedRoute,  fb: FormBuilder,private router: Router,private preOrderService:PreOrderService) {
        this.myForm = fb.group({
            'contactsName': ['', Validators.required],
            'contactsTel': ['', Validators.required],
            'startDate': [''],
            'startTime': ['', Validators.required],
            'endTime': ['', Validators.required],
            'orderState': ['', Validators.required],
            'orderId': ['', ''],
        });
        this.ngOnInit();
        this.orderStates=[{value:'00',name:'无意向'},{value:'01',name:'待处理'},{value:'02',name:'继续跟进'},{value:'03',name:'锁定一天'},{value:'04',name:'生成订单'}];
    }
    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => { this.orderId = params["orderId"]}
        );
        this.queryPreOrder();

    }

    queryPreOrder(){
        this.httpService.httpGetWithAuth("preOrder/getInfo?orderId="+this.orderId).then(restEntity => {
            if (restEntity.status ==-1) {
                    console.log(restEntity.msg, "获取失败");
                    return;
                }else{
                 this.preOrder = restEntity.object;
                 this.orderState = this.preOrder.orderState;
                 this.datePickerComponent.model=this.preOrder.startDate;
                 console.log(restEntity.object)
                this.httpService.httpGetWithAuth("preOrder/track/list?preOrderId="+this.orderId).then(restEntity => {
                    if (restEntity.status ==-1) {
                        console.log(restEntity.msg, "获取失败");
                        return;
                    }else{
                        this.preOrderTracks = restEntity.object.results;
                    }
                }).catch(
                    error => {
                        console.log("获取失败")
                    }
                );
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
    }
    editPreOrder(value: string){
        let dateObj = eval(this.datePickerComponent.model);
        console.log(dateObj);
        if (dateObj != null && dateObj.year) {
            this.datePickerComponent.model = dateObj.year + '-' + dateObj.month + '-' + dateObj.day;
            let orderObj = eval(value);
            orderObj.startDate = this.datePickerComponent.model;
        } else {
            let orderObj = eval(value);
            orderObj.startDate = this.datePickerComponent.model;

        }
        console.log(value);
        this.preOrderService.editPreOrder(value).then(restEntity => {
            if (restEntity.status ==1) {
                this.router.navigate(['/pre-order']);
            }
        }).catch(
            error => {
                console.log("更新失败")
            }
        );
    }

    datefmt(){
        let dateObj = eval(this.datePickerComponent.model);
        this.datePickerComponent.model = dateObj.year+'-'+dateObj.month+'-'+dateObj.day;
    }
}
