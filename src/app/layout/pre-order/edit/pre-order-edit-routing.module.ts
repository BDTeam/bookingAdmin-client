import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PreOrderEditComponent} from "./pre-order-edit.component";

const routes: Routes = [
    { path: 'pre-order-edit/:orderId', component: PreOrderEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreOrderEditRoutingModule { }
