import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../../services/UserService";
import {MerchantAccount} from "../../../../domain/MerchantAccount";

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.scss'],
    animations: [routerTransition()]
})
export class UserEditComponent implements OnInit {
    myForm: FormGroup;
    user?:MerchantAccount;
    userId?:number;
    username?:string;
    states=[];
    state?:string;
    constructor( fb: FormBuilder,private activeRoute: ActivatedRoute,private userService: UserService,public router: Router,) {
        this.myForm = fb.group({
            'username': ['', [Validators.required,Validators.minLength(4),Validators.maxLength(8)]],
            'password': ['', [Validators.minLength(4),Validators.maxLength(8)]],
            'id': [''],
            'state': [''],
        });
    }
    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => { this.userId = params["userId"]}
        );
        this.selectOneUser();
        this.states=[{value:'0',name:'启用'},{value:'1',name:'停用'},];
        this.myForm.valueChanges.subscribe(data => this.onValueChanged(data));
    }
    //存储错误信息
    formErrors = {
        'username': '',
        'password': '',
    };
    //错误对应的提示
    validationMessages = {
        'username': {
            'required': '用户名必填.',
            'minlength': '至少4个字符',
            'maxlength': '最多8个字符',
        },
        'password':{
            'minlength': '至少4个字符',
            'maxlength': '最多8个字符',
        },
    };
    /**
     * 表单值改变时，重新校验
     * @param data
     */
    onValueChanged(data) {
        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            //取到表单字段
            const control = this.myForm.get(field);
            //表单字段已修改或无效
            if (control && control.dirty && !control.valid) {
                //取出对应字段可能的错误信息
                const messages = this.validationMessages[field];
                //从errors里取出错误类型，再拼上该错误对应的信息
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + '';
                }
            }

        }

    }

    selectOneUser(){
        let param =[{key:"accountId",value:this.userId}];
        // "userId=" + this.user.id;
        this.userService.selectOneUser(param).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.user = restEntity.object;
                this.username = this.user.username;
                this.userId = this.user.id;
                this.state = this.user.state;
                console.log(restEntity.object);
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
    }

    updateUser(value:string){
        console.log(value);
        this.userService.updateUser(value).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "更新失败");
                return;
            }else{
                console.log(restEntity.object);
                this.router.navigate(['/user']);
            }
        }).catch(
            error => {
                console.log("更新失败")
            }
        );
    }

}
