import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../router.animations";
import {MerchantAccount} from "../../../domain/MerchantAccount";
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {UserService} from "../../../services/UserService";

@Component({
    selector: 'app-user-page',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    animations: [routerTransition()]
})
export class UserComponent implements OnInit {
    users:MerchantAccount[];
    params: Array<{ key: string, value: any }>;//查询条件
    username?:string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    constructor(private userService:UserService) {

    }

    ngOnInit() {
       this.searchList();
    }

    searchList() {
        if(this.username!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "userLogin", value: this.username},{key: "page", value: pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.userService.selectByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.users = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }
}
