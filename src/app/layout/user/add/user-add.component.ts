import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../../services/UserService";

@Component({
    selector: 'app-user-add',
    templateUrl: './user-add.component.html',
    styleUrls: ['./user-add.component.scss'],
    animations: [routerTransition()]
})
export class UserAddComponent implements OnInit {
    myForm: FormGroup;
    states=[];
    state:string="0";
    constructor( fb: FormBuilder,private activeRoute: ActivatedRoute,private userService: UserService,public router: Router, ) {
        this.myForm = fb.group({
            'username': ['', [Validators.required,Validators.minLength(4),Validators.maxLength(8)]],
            'password': ['', [Validators.required,Validators.minLength(4),Validators.maxLength(8)]],
            'state': [],
        });
    }
    ngOnInit() {
        this.myForm.valueChanges.subscribe(data => this.onValueChanged(data));
        this.states=[{value:'0',name:'启用'},{value:'1',name:'停用'},];
    }
    //存储错误信息
    formErrors = {
        'username': '',
        'password': '',
    };
    //错误对应的提示
    validationMessages = {
        'username': {
            'required': '用户名必填.',
            'minlength': '至少4个字符',
            'maxlength': '最多8个字符',
        },
        'password':{
            'required': '请输入密码',
            'minlength': '至少4个字符',
            'maxlength': '最多8个字符',
        },
    };
    /**
     * 表单值改变时，重新校验
     * @param data
     */
    onValueChanged(data) {
        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            //取到表单字段
            const control = this.myForm.get(field);
            //表单字段已修改或无效
            if (control && control.dirty && !control.valid) {
                //取出对应字段可能的错误信息
                const messages = this.validationMessages[field];
                //从errors里取出错误类型，再拼上该错误对应的信息
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + '';
                }
            }

        }

    }



    addUser(value:string){
         console.log(value);
         this.userService.addUser(value).then(restEntity => {
             if (restEntity.status ==-1) {
                 console.log(restEntity.msg, "添加失败");
                 return;
             }else{
                 console.log(restEntity.object);
                 this.router.navigate(['/user']);
             }
         }).catch(
             error => {
                 console.log("添加失败")
             }
         );
    }

}
