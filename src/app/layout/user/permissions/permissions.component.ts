import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../../services/UserService";
import {Role} from "../../../../domain/Role";

@Component({
    selector: 'app-permissions',
    templateUrl: './permissions.component.html',
    styleUrls: ['./permissions.component.scss'],
    animations: [routerTransition()]
})
export class PermissionsComponent implements OnInit {
    myForm: FormGroup;
    accountId:string="";
    roleId:string="";
    userRoleId:string="";
    roles:Role[];
    role:Role;
    constructor( fb: FormBuilder,private activeRoute: ActivatedRoute,private userService: UserService,public router: Router,) {
        this.myForm = fb.group({
            'accountId': [],
            'roleId': ['', Validators.required],
            'userRoleId': [],
        });
    }
    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => { this.accountId = params["userId"]}
        );
        this.selectRoles();

    }
    selectRoles(){
        let params=[];
        this.userService.selectRolesByCondition(params).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取角色失败");
                return;
            }else{
                console.log(restEntity.object);
                this.roles = restEntity.object;
                let param =[{key:"accountId",value:this.accountId}];
                this.userService.roleInfo(param).then(restEntity => {
                    if (restEntity.status ==-1) {
                        console.log(restEntity.msg, "获取失败");
                        return;
                    }else{
                        this.role = restEntity.object;
                        this.roleId = this.role.roleId+"";
                        console.log(restEntity.object);
                    }
                }).catch(
                    error => {
                        console.log("获取失败")
                    }
                );
            }
        }).catch(
            error => {
                console.log("获取角色失败")
            }
        );
    }

    accreditRole(value:string){
        let params =[{key:"accountId",value:this.accountId},{key:"roleId",value:this.roleId}];
        this.userService.accreditRole(params).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "授权失败");
                return;
            }else{
                this.router.navigate(['/user']);
            }
        }).catch(
            error => {
                console.log("授权失败")
            }
        );
    }

}
