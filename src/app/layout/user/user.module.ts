import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
import {UserComponent} from "./user.component";
import {PermissionsModule} from "./permissions/permissions.module";
import {UserEditModule} from "./edit/user-edit.module";
import {UserAddModule} from "./add/user-add.module";

@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        FormsModule,
        PageHeaderModule,
        ReactiveFormsModule,
        PaginationModule,
        PermissionsModule,
        UserEditModule,
        UserAddModule,
    ],
    declarations: [UserComponent]
})
export class UserModule {
}
