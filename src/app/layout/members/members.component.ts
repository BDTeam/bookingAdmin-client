import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {WPUser} from "../../../domain/WPUser";
import {MembersService} from "../../../services/MembersService";

@Component({
    selector: 'app-members',
    templateUrl: './members.component.html',
    styleUrls: ['./members.component.scss'],
    animations: [routerTransition()]
})
export class MembersComponent implements OnInit {
    data: WPUser[];
    params: Array<{ key: string, value: any }>;//查询条件
    userLogin?: string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    constructor(private membersService: MembersService) {
    }
    ngOnInit() {
        this.searchList();
    }
    searchList() {
        if(this.userLogin!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "userLogin", value: this.userLogin},{key: "page", value:pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.membersService.selectByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.data = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }

    selectPage() {
        console.log(this.paginationComponent)
        this.searchList();
    }
}
