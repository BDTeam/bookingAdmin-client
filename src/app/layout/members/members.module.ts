import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MembersComponent } from './members.component';
import { MembersRoutingModule } from './members-routing.module';
import { PageHeaderModule } from './../../shared';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule ,
        MembersRoutingModule,
        PageHeaderModule,
        // ngPaginationModule,
        PaginationModule,
    ],
    declarations: [MembersComponent]
})
export class MembersModule { }
