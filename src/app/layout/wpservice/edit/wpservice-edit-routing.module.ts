import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WpserviceEditComponent} from "./wpservice-edit.component";

const routes: Routes = [
    { path: 'wpservice-edit/:wpserviceId', component: WpserviceEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WpserviceEditRoutingModule { }
