import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {WpserviceEditComponent} from "./wpservice-edit.component";

describe('WpserviceEditComponent', () => {
  let component: WpserviceEditComponent;
  let fixture: ComponentFixture<WpserviceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [
      RouterTestingModule,
      PageHeaderModule,
    ],
      declarations: [ WpserviceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WpserviceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
