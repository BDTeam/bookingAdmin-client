import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WpserviceEditRoutingModule } from './wpservice-edit-routing.module';
import { WpserviceEditComponent } from './wpservice-edit.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        WpserviceEditRoutingModule,
        PageHeaderModule,
    ],
    declarations: [WpserviceEditComponent]
})
export class WpserviceEditModule { }
