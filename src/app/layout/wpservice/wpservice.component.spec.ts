import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WpserviceComponent } from './wpservice.component';

describe('WpserviceComponent', () => {
  let component: WpserviceComponent;
  let fixture: ComponentFixture<WpserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WpserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WpserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
