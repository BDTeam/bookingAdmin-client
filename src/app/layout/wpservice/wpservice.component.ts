import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../router.animations";
import {MerchantAccount} from "../../../domain/MerchantAccount";
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {WPService} from "../../../domain/WPService";
import {OrderService} from "../../../services/OrderService";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
    selector: 'app-wpservice-page',
    templateUrl: './wpservice.component.html',
    styleUrls: ['./wpservice.component.scss'],
    animations: [routerTransition()]
})
export class WpserviceComponent implements OnInit {
    myForm: FormGroup;
    wpservices:WPService[];
    params: Array<{ key: string, value: any }>;//查询条件
    serviceName?:string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    constructor(private orderService:OrderService,private fb: FormBuilder,public router: Router,) {
        this.myForm = fb.group({
            'serviceName': ['', Validators.required],
            // 'serviceType': ['', Validators.required],
            // 'servicePrice': ['', Validators.required],
        });
    }
    ngOnInit() {
       this.searchList();
    }
    searchList() {
        if(this.serviceName!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "serviceName", value: this.serviceName},{key: "page", value: pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.orderService.selectServicesByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                console.log(restEntity.object.results);
                this.wpservices = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }
    selectPage() {
        this.searchList();
    }

    delService(value:any){
        console.log(value);
        if(value){
            this.params = [{key: "id", value: value}];
            this.orderService.delServie(this.params).then(restEntity => {
                if (restEntity.status ==-1) {
                    console.log(restEntity.msg, "删除失败");
                    return;
                }else{
                    this.searchList();
                    // console.log(restEntity.object);
                    // this.router.navigate(['/wpservice']);
                }
            }).catch(
                error => {
                    console.log("删除失败")
                }
            );
        }

    }
}
