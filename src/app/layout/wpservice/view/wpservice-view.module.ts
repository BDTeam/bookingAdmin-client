import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WpserviceViewRoutingModule } from './wpservice-view-routing.module';
import { WpserviceViewComponent } from './wpservice-view.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        WpserviceViewRoutingModule,
        PageHeaderModule,
    ],
    declarations: [WpserviceViewComponent]
})
export class WpserviceViewModule { }
