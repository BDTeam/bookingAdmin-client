import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../../../../services/OrderService";
import {WPService} from "../../../../domain/WPService";

@Component({
    selector: 'app-wpservice-view',
    templateUrl: './wpservice-view.component.html',
    styleUrls: ['./wpservice-view.component.scss'],
    animations: [routerTransition()]
})
export class WpserviceViewComponent implements OnInit {
    myForm: FormGroup;
    wpservice?:WPService={};
    serviceId?:number;
    serviceName?:string;
    servicePrice?:number;
    serviceTypes=[];
    serviceType:string="0";
    constructor( fb: FormBuilder,private activeRoute: ActivatedRoute,private orderService: OrderService,public router: Router,) {
    }
    ngOnInit() {
        this.activeRoute.params.subscribe(
            params => { this.serviceId = params["wpserviceId"]}
        );
        this.selectOneWpservice();
        this.serviceTypes=[{value:'0',name:'增值服务'},{value:'2',name:'用餐'},];
    }
    selectOneWpservice(){
        let param =[{key:"id",value:this.serviceId}];
        // "wpserviceId=" + this.wpservice.id;
        this.orderService.selectOneWpservice(param).then(restEntity => {
            if (restEntity.status ==-1) {
                console.log(restEntity.msg, "获取失败");
                return;
            }else{
                this.wpservice = restEntity.object;
                this.serviceName = this.wpservice.service_name;
                this.serviceId = this.wpservice.id;
                this.serviceType = this.wpservice.service_type;
                this.servicePrice = this.wpservice.service_price;
                console.log(restEntity.object);
            }
        }).catch(
            error => {
                console.log("获取失败")
            }
        );
    }
}
