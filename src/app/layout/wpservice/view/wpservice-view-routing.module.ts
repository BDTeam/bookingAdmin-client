import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WpserviceViewComponent} from "./wpservice-view.component";

const routes: Routes = [
    { path: 'wpservice-view/:wpserviceId', component: WpserviceViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WpserviceViewRoutingModule { }
