import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WpserviceRoutingModule} from './wpservice-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
import {WpserviceComponent} from "./wpservice.component";
import {WpserviceAddModule} from "./add/wpservice-add.module";
import {WpserviceEditModule} from "./edit/wpservice-edit.module";
import {WpserviceViewModule} from "./view/wpservice-view.module";
@NgModule({
    imports: [
        CommonModule,
        WpserviceRoutingModule,
        FormsModule,
        PageHeaderModule,
        ReactiveFormsModule,
        PaginationModule,
        WpserviceAddModule,
        WpserviceEditModule,
        WpserviceViewModule,
    ],
    declarations: [WpserviceComponent]
})
export class WpserviceModule {
}
