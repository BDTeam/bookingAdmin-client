import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WpserviceAddComponent} from "./wpservice-add.component";

const routes: Routes = [
    { path: 'wpservice-add', component: WpserviceAddComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WpserviceAddRoutingModule { }
