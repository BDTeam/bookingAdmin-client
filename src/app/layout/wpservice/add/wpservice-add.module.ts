import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WpserviceAddRoutingModule } from './wpservice-add-routing.module';
import { WpserviceAddComponent } from './wpservice-add.component';
import {PageHeaderModule} from "../../../shared/modules/page-header/page-header.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        WpserviceAddRoutingModule,
        PageHeaderModule,
    ],
    declarations: [WpserviceAddComponent]
})
export class WpserviceAddModule { }
