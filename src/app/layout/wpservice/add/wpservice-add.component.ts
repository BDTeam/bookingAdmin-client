import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from "../../../router.animations";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../../../../services/OrderService";

@Component({
    selector: 'app-wpservice-add',
    templateUrl: './wpservice-add.component.html',
    styleUrls: ['./wpservice-add.component.scss'],
    animations: [routerTransition()]
})
export class WpserviceAddComponent implements OnInit {
    myForm: FormGroup;
    serviceTypes=[];
    serviceType:string="0";
    constructor( fb: FormBuilder,private activeRoute: ActivatedRoute,private orderService: OrderService,public router: Router, ) {
        this.myForm = fb.group({
            'service_name': ['', [Validators.required,Validators.minLength(2),Validators.maxLength(8)]],
            'service_type': ['', [Validators.required,]],
            'service_price':['', [Validators.required,Validators.pattern("^\\d+(\\.\\d+)?$")]],
        });
    }
    ngOnInit() {
        this.myForm.valueChanges.subscribe(data => this.onValueChanged(data));
        this.serviceTypes=[{value:'0',name:'增值服务'},{value:'2',name:'用餐'},];
    }
    //存储错误信息
    formErrors = {
        'service_name': '',
        'service_type': '',
        'service_price': '',
    };
    //错误对应的提示
    validationMessages = {
        'service_name': {
            'required': '服务名必填.',
            'minlength': '至少2个字符',
            'maxlength': '最多8个字符',
        },
        'service_type':{
            'required': '请选择',
        },
        'service_price':{
            'required': '请输入价格',
            'pattern': '格式有误',
        },
    };
    /**
     * 表单值改变时，重新校验
     * @param data
     */
    onValueChanged(data) {
        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            //取到表单字段
            const control = this.myForm.get(field);
            //表单字段已修改或无效
            if (control && control.dirty && !control.valid) {
                //取出对应字段可能的错误信息
                const messages = this.validationMessages[field];
                //从errors里取出错误类型，再拼上该错误对应的信息
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + '';
                }
            }

        }

    }



    addWpservice(value:string){
         console.log(value);
         this.orderService.addWPService(value).then(restEntity => {
             if (restEntity.status ==-1) {
                 console.log(restEntity.msg, "添加失败");
                 return;
             }else{
                 console.log(restEntity.object);
                 this.router.navigate(['/wpservice']);
             }
         }).catch(
             error => {
                 console.log("添加失败")
             }
         );
    }

}
