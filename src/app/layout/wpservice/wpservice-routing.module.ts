import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WpserviceComponent } from './wpservice.component';

const routes: Routes = [
    { path: '', component: WpserviceComponent
    }


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WpserviceRoutingModule { }
