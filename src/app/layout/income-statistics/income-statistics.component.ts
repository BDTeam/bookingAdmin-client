import {Component, OnInit, ViewChild} from '@angular/core';
import {PaginationComponent} from "../../components/ngPagination/pagination.component";
import {routerTransition} from "../../router.animations";
import {IncomeService} from "../../../services/IncomeService";
import {OrderStatisca} from "../../../domain/OrderStatisca";

@Component({
    selector: 'app-income-statistics-page',
    templateUrl: './income-statistics.component.html',
    styleUrls: ['./income-statistics.component.scss'],
    animations: [routerTransition()]
})
export class IncomeStatisticsComponent implements OnInit {
    data: OrderStatisca[];
    params: Array<{ key: string, value: any }>;//查询条件
    startDate?: string="";
    endDate?: string="";
    @ViewChild(PaginationComponent) paginationComponent: PaginationComponent;
    constructor(private incomeService:IncomeService) {

    }

    ngOnInit() {
        this.searchList();
    }
    searchList() {
        if(this.startDate!=''||this.endDate!=''){
            this.paginationComponent.defaultPagination=0;
        }
        let pageStart = this.paginationComponent.defaultPagination>0?this.paginationComponent.defaultPagination * this.paginationComponent.paginationSize - this.paginationComponent.paginationSize:0;
        this.params = [{key: "startDate", value: this.startDate},{key: "endDate", value: this.endDate},{key: "page", value: pageStart},{key: "size", value: this.paginationComponent.paginationSize}];
        this.incomeService.selectIncomeStatiscaByCondition(this.params).then(restEntity => {
            if (restEntity.status == -1) {
                console.log(restEntity.msg);
            } else {
                this.data = restEntity.object.results;
            }
        }).catch(
            error => console.log(error)
        )
    }
    selectPage() {
        this.searchList();
    }

}
