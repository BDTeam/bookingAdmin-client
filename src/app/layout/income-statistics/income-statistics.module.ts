import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IncomeStatisticsComponent} from './income-statistics.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PageHeaderModule} from "../../shared/modules/page-header/page-header.module";
import {PaginationModule} from "../../components/ngPagination/pagination.module";
import {IncomeStatisticsRoutingModule} from "./income-statistics-routing.module";
import {DatePickerModule} from "../../components/date/date-picker.module";

@NgModule({
    imports: [
        CommonModule,
        IncomeStatisticsRoutingModule,
        FormsModule,
        PageHeaderModule,
        ReactiveFormsModule,
        PaginationModule,
        DatePickerModule,
    ],
    declarations: [IncomeStatisticsComponent]
})
export class IncomeStatisticsModule {
}
