import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncomeStatisticsComponent } from './income-statistics.component';

const routes: Routes = [
    { path: '', component: IncomeStatisticsComponent
    }


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IncomeStatisticsRoutingModule { }
