export class WPService{
    public  id?:number;
    /**
     * 服务名
     */
    public service_name?:string;
    /**
     * 服务类型(1.基本服务，0.增值服务,2.用餐类型)
     */
    public service_type?:string;
    /**
     * 创建时间
     */
    public create_time?:string;
    /**
     * 是否必选服务
     */
    public service_required?:string;
    /**
     * 价格
     */
    public service_price?:number;
    /**
     * service_sales
     */
    public service_sales?:string;
    /**
     * 服务信息
     */
    public service_detail?:string;
    /**
     * 图片
     */
    public service_pic?:string;
    /**
     * 所需用户级别
     */
    public user_level?:number;
    /**
     * 排序
     */
    public sort?:string;
    /**
     * 图标
     */
    public icon?:string;
    /**
     * 是否允许用户自定义预定数量
     */
    public service_count_userdefined?:number;
    /**
     * 服务项目可选最大数量（默认1）
     */
    public service_count_max?:number;
    /**
     * 服务项目可选最小数量（默认1）
     */
    public service_count_min?:number;
    constructor(){

    }
}
