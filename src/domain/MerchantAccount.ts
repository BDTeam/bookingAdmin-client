export class MerchantAccount{
    /**
     * 商户账户ID
     */
    public id?:number;
    /**
     * 商户登陆账号
     */
    public username?:string;
    /**
     * 商户登陆密码
     */
    public password?:string;
    /**
     * 状态
     */
    public state?:string;
    /**
     * 创建时间
     */
    public createTime?:number;
    /**
     * 创建人
     */
    public createUserid?:number;
    /**
     * 修改时间
     */
    public updateTime?:number;
    /**
     * 修改人
     */
    public updateUserid?:number;

    public merchantId?:number;

    constructor(){

    }
}
