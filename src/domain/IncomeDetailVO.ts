export class IncomeDetailVO{
    public orderDate:string; //预定日期
    public timeQuantum:string; //预定时间段
    public personNum:number ; //预定人数
    public orderRemark:string; //订单备注
    public contactName:string; //联系人
    public contactPhone:string; //联系电话
    public realIncome:number;//实际收入
    public earnestAmount:number; //定金
    public retainageAmount:number;//尾款
    public poundageAmount:number; //手续费
    public commissionAmount:number;//佣金
    public earnestPayMethod:string;//定金支付方式
    public retainagePayMethod:string;//尾款支付方式
    public orderChannel:string;//订单渠道来源
    constructor(){

    }
}
