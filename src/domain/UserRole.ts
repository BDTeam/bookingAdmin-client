export class UserRole{
    public userRoleId:number;
    /**
     * 用户ID
     */
    public userId:number;
    /**
     * 角色ID
     */
    public roleId:number;
}
