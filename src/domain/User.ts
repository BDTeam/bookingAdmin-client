export class User{
    public uid?:number;
    public username?:string;
    public userToken?:string;
    authorities:any[]=[];
}
