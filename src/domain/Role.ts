export class Role{
    public  roleId:number;
    /**
     * 角色名称
     */
    public roleName:string;
    /**
     * 角色类型
     */
    public roleType:string;

    public status:number;
    /**
     * 创建时间
     */
    public creationDate:string;
    /**
     * 创建人
     */
    public createdBy:number;
    /**
     * 最后更新时间
     */
    public lastUpdateDate:string;
    /**
     * 最后更新人
     */
    public lastUpdatedBy:number;
}
