export class PreOrder{
    public orderId?:number;
    /***
     * 联系人
     */
    public contactsName?:string;
    /***
     *联系电话
     */
    public contactsTel?:string;
    /***
     *人数
     */
    public nums?:number;
    /***
     *开始时间
     */
    public startTime?:string;
    /***
     *开始日期
     */
    public startDate?:string;
    /***
     *结束日期
     */
    public endDate?:string;
    /***
     *结束时间
     */
    public endTime?:string;
    /***
     *创建者id
     */
    public createId?:number;
    /***
     *创建者
     */
    public createName?:string;
    /***
     *创建时间
     */
    public createTime?:string;
    /***
     *订单状态（生成预订单：01、无意向：00、继续跟进：02、锁定一天：03）
     */
    public orderState?:string;
    /***
     *处理者id
     */
    public dealId?:number;
    /***
     *处理者
     */
    public dealName?:string;
    /***
     *备注
     */
    public mark?:string;
    constructor() {
    }
}
