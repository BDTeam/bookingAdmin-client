export class PreOrderTrack{
    public id?:number;
    /**
     * 预订单ID
     */
    public preOrderId?:number;

    /**
     * 跟踪信息
     */
    public trackInfo?:string;

    /**
     * 商户ID
     */
    public merchantId?:number;

    /**
     * 创建人ID
     */
    public createId?:number;

    /**
     * 创建人名称
     */
    public createName?:string;

    /**
     * 创建时间
     */
    public createTime?:string;

    constructor(){
    }
}
