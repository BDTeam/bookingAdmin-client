export class Order{
    /**
     * 编号
     */
    public id?:number;

    /**
     * 预定日期
     */
    public orderTime?:string;

    /**
     * 预定电话
     */
    public phone?:string;
    public createTime?:string;

    /**
     * 预定人
     */
    public userName?:string;

    /**
     * 预定人id
     */
    public userId?:number;

    /**
     * 订单状态：0(已取消)10(默认)?:未支付,20?:已支付,30?:已完成
     */
    public state?:string;

    /**
     * 是否用餐（1.是，0.否）
     */
    public diningStat?:string;

    /**
     * 参加人数
     */
    public diningNum?:number;

    /**
     * 场次
     */
    public orderEvent?:number;

    /**
     * 订单金额
     */
    public orderAmount?:number;

    /**
     * 优惠券ID
     */
    public cardId?:string;

    /**
     * 优惠券CODE
     */
    public cardCode?:string;

    /**
     * 订单类型（0?:线上预定,1?:电话预定）
     */
    public orderType?:number;

    /**
     * 是否已赠送积分 0否 1是
     */
    public sendPoints?:number;

    /**
     * 开始时间
     */
    public startTime?:string;

    /**
     * 结束时间
     */
    public endTime?:string;

    /**
     * 是否为节假日,0否 1是
     */
    public isHoliday?:string;

    /**
     * 订单号
     */
    public orderSn?:string;

    /**
     * 定金预交情况（下拉菜单：工作日团购卷258/人，周末团
     */
    public depositType?:string;

    /**
     * 金额或团劵数量
     */
    public moneyOrTickets?:number;
    /**
     * 商户ID
     */
    public merchantId?:number;
    /***
     *订单备注
     */
    public orderRemark?:string;
    /***
     *订单定金
     */
    public earnestAmount?:number;
    /***
     *订单手续费
     */
    public poundageAmount?:number;
    /***
     *尾款金额
     */
    public retainageAmount?:number;
    /***
     *佣金金额
     */
    public commissionAmount?:number;
    /***
     *大众点评 百场汇 自访 百度糯米
     * 'dzdp','bch','nm','khzf'
     */
    public orderChannel?:string;
    /***
     * 'wx','zfb','pos','xj','dg','khzf','bch','dzdp','nm'
     *定金支付方式 微信  支付宝 pos 现金 对公收款 大众点评 百场汇 自访 百度糯米
     */
    public earnestPayMethod?:string;
    /***
     * 'wx','zfb','pos','xj','dg','khzf','bch','dzdp','nm'
     *尾款支付方式 微信  支付宝 pos 现金 对公收款 大众点评 百场汇 自访 百度糯米
     */
    public retainagePayMethod?:string;


    constructor(){

    }
}

