export class WPUser {
    public id: number;
    public userLogin: string;
    public userPass: string;
    public userNicename?: string;
    public userEmail?: string;
    public userUrl?: string;
    public userRegistered?: string;
    public userActivationKey?: string;
    public userStatus?: number;
    public displayName?: string;
    /**
     * 用户冻结积分
     */
    public userFrozenPoints?: number;
    /**
     * 1 会员 2销售员 3一般股东 4干股股东
     */
    public userType?: number;
    /**
     * 用户等级
     */
    public userLevel?: number;
    /**
     * 用户积分
     */
    public userPoints?: string;
    /**
     * 推荐人编号
     */
    public userRecommendId?: number;
    /**
     * 推荐人名称
     */
    public userRecommendName?: string;
    /**
     * 0否 1是
     */
    public isAdmin?: number;
    public token?:string;
    public userRegisteredStr?:string;
    constructor() {
    }
}
