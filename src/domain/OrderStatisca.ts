export class OrderStatisca{
    /**
     * 统计类型
     */
    public sType:string;

    /**
     * 统计总金额
     */
    public sTotal:number;

    /**
     * 定金总额
     */
    public sEarnestMoney:number;

    /**
     * 佣金总额
     */
    public sCommission:number;
    /**
     * 手续费总额
     */
    public sPoundage:number;
    /**
     * 尾款总额
     */
    public sRetainage:number;
    /**
     * 统计时间
     */
    public sCreatetime:string;
    constructor(){
    }
}
