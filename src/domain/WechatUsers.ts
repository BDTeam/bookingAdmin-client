export class WechatUsers{
    /**
     * 微信用户ID
     */
    public id?:number;
    /**
     * 微信用户OPENID
     */
    public  openId?:string;

    /**
     * 微信用户昵称
     */
    public nickName?:string;

    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    public sex?:string;

    /**
     * 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效
     */
    public avatar?:string;

    /**
     * 语言
     */
    public language?:string;

    /**
     * 城市
     */
    public city?:string;

    /**
     * 省份
     */
    public province?:string;

    /**
     * 国家
     */
    public country?:string;

    /**
     * 商户ID
     */
    public merchantId?:number;

    /**
     * 创建时间
     */

    public createTime?:string;
    /**
     * 备注
     */
    public remark?:string;
    constructor(){

    }
}
