import {Injectable} from "@angular/core";
import {HttpService} from "../providers/http-service";
/**
 * Created by pc on 2017/8/9.
 */
@Injectable()
export class MembersService {
    constructor(private httpService :HttpService) {
    }
    public selectByCondition(params:Array<{key:string,value:any}>){
        let url:string = "user/list?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }

    public selectWxMembersByCondition(params:Array<{key:string,value:any}>){
        let url:string = "wxuser/list?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }

    public sendMsg(value: string){
        let url:string = "wxuser/sendMsg";
        return this.httpService.httpPostWithAuth(url,value);
    }

    public setRemark(value: string){
        let url:string = "wxuser/setRemark";
        return this.httpService.httpPostWithAuth(url,value);
    }

    public selectWxMembersById(wxuserId:any){
        let url:string = "wxuser/getInfo?1=1&wxuserId="+wxuserId;
        return this.httpService.httpGetWithAuth(url);
    }
    public weChatSync(params:Array<{key:string,value:any}>){
        let url:string = "wxuser/synchronize?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }


}
