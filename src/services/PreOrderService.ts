import {Injectable} from "@angular/core";
import {HttpService} from "../providers/http-service";
@Injectable()
export class PreOrderService{
    constructor(private httpService :HttpService) {
    }
    public selectByCondition(params:Array<{key:string,value:any}>){
        let url:string = "preOrder/list?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }

    public editPreOrder(value: any){
        return this.httpService.httpPostWithAuth("preOrder/modify", value);
    }
    public delPreOrder(params:Array<{key:string,value:any}>){
        let url:string = "preOrder/delete?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }
}
