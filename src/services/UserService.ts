import {Injectable} from "@angular/core";
import {HttpService} from "../providers/http-service";
@Injectable()
export class UserService{
    constructor(private httpService :HttpService){

    }

    /****
     * 获取用户列表
     * @param params
     * @returns {Promise<RestEntity>}
     */
    public selectByCondition(params:Array<{key:string,value:any}>){
        let url:string = "account/list?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }


    /***
     * 添加新用户
     * @param params
     * @returns {Promise<RestEntity>}
     */
    public addUser(value:string){
        let url:string = "account/add";
        return this.httpService.httpPostWithAuth(url,value);
    }
    /***
     * 添加新用户
     * @param params
     * @returns {Promise<RestEntity>}
     */
    public updateUser(value:string){
        let url:string = "account/modify";
        return this.httpService.httpPostWithAuth(url,value);
    }
    /***
     * 根据条件获取用户
     * @param params
     * @returns {Promise<RestEntity>}
     */
    public selectOneUser(params:Array<{key:string,value:any}>){
        let url:string = "account/getInfo?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }


    /***
     * 获取角色列表
     * @param params
     * @returns {Promise<RestEntity>}
     */
    public selectRolesByCondition(params:Array<{key:string,value:any}>){
        let url:string = "roles/list?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }

    /****
     * 授权
     */
    public accreditRole(params:Array<{key:string,value:any}>){
        let url:string = "account/doAccreditRole?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }
    /****
     *获取用户权限
     */
    public roleInfo(params:Array<{key:string,value:any}>){
        let url:string = "roles/getInfo?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }
}
