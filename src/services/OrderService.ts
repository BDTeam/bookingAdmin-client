import {Injectable} from "@angular/core";
import {HttpService} from "../providers/http-service";
@Injectable()
export class OrderService {
    constructor(private httpService :HttpService) {
    }
    public selectByCondition(params:Array<{key:string,value:any}>){
        let url:string = "order/list?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }

    public addOrder(value: string){
        let url:string = "order/add";
        return this.httpService.httpPostWithAuth(url,value);
    }

    public editOrder(value: string){
        let url:string = "order/modify";
        return this.httpService.httpPostWithAuth(url,value);
    }
    public delOrder(params:Array<{key:string,value:any}>){
        let url:string = "order/delete?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }


    public selectServicesByCondition(params:Array<{key:string,value:any}>){
        let url:string = "wPService/list?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }


    public addWPService(value: string){
        let url:string = "wPService/add";
        return this.httpService.httpPostWithAuth(url,value);
    }

    public editWPServicer(value: string){
        let url:string = "wPService/modify";
        return this.httpService.httpPostWithAuth(url,value);
    }
    public selectOneWpservice(params:Array<{key:string,value:any}>){
        let url:string = "wPService/getInfo?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }

    public delServie(params:Array<{key:string,value:any}>){
        let url:string = "wPService/delete?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }
}
