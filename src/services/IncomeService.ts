import {HttpService} from "../providers/http-service";
import {Injectable} from "@angular/core";
@Injectable()
export class IncomeService {
    constructor(private httpService: HttpService) {
    }
    public selectIncomeStatiscaByCondition(params:Array<{key:string,value:any}>){
        let url:string = "statisca/incomeStatisca?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }

    public selectIncomeDetailByCondition(params:Array<{key:string,value:any}>){
        let url:string = "statisca/incomeDetail?1=1";
        if(params!=null&&params.length>0){
            for(let param of params ){
                url = url +"&&"+param.key + "=" +param.value;
            }
        }
        return this.httpService.httpGetWithAuth(url);
    }
}
